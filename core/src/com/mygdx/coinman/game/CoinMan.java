package com.mygdx.coinman.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.ScreenUtils;

public class CoinMan extends ApplicationAdapter {
	SpriteBatch batch;
	Texture background; // for background image
	Texture[] man;
	int manState=0, pause=0;
	float gravity=0.2f;
	float velocity = 0;
	int manY=0;
	@Override
	public void create () {
		batch = new SpriteBatch();
		background = new Texture("bg.png");
		man = new Texture[4];
		man[0] = new Texture("frame1.png");
		man[1] = new Texture("frame2.png");
		man[2] = new Texture("frame3.png");
		man[3] = new Texture("frame4.png");
		manY = Gdx.graphics.getHeight()/ 2;
	}

	@Override
	public void render () {
		batch.begin(); //starts
		if (Gdx.input.isTouched()){
			velocity =-10;
		}
		if(pause<8){ //to slow down the speed
			pause++;
		}else{
			pause=0;
			if (manState<3){ //to make him show running
				manState++;
			}else{
				manState=0;
			}
		}
		velocity = velocity+gravity;
		manY = (int) (manY - velocity);
		if(manY<=0){
			manY=0;
		}
		batch.draw(background,0,0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		batch.draw(man[manState], Gdx.graphics.getWidth()/ 2 - man[manState].getWidth()/2 - 5, manY);
		batch.end(); //ends the work cycle
	}
	
	@Override
	public void dispose () {
		batch.dispose();

	}
}
